package com.example.comicfirebase;

import com.example.comicfirebase.models.Comic;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ApiFirebase {
    public static FirebaseDatabase database;
    public static DatabaseReference databaseRef;
    FirebaseRecyclerOptions<Comic> options;

    public ApiFirebase(String nameDb) {
        database = FirebaseDatabase.getInstance();
        databaseRef = database.getReference(nameDb);
        options = new FirebaseRecyclerOptions.Builder<Comic>()
                .setQuery(databaseRef, Comic.class).build();
    }

    public void insert(Comic c) {
        String key = databaseRef.push().getKey();
        assert key != null;
        c.setId(key);
        databaseRef.child(key).setValue(c);
    }

    public void update(Comic c) {
        databaseRef.child(c.getId()).setValue(new Comic(c.getId(), c.getTitle(), c.getAuthor(), c.getImg(),c.getRating()));
        //idem
        /*databaseRef.child(c.getId()).child(Comic.attrTitle).setValue(c.getTitle());
        databaseRef.child(c.getId()).child(Comic.attrAuthor).setValue(c.getAuthor());
        databaseRef.child(c.getId()).child(Comic.attrImg).setValue(c.getImg());
        databaseRef.child(c.getId()).child(Comic.attrRating).setValue(c.getRating());*/
    }

    public void delete(Comic c) {
        databaseRef.child(c.getId()).removeValue();
    }

}
