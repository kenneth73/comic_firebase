package com.example.comicfirebase;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.comicfirebase.adapters.ComicAdapter;
import com.example.comicfirebase.models.Comic;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import static com.google.android.material.snackbar.Snackbar.*;

public class MainActivity extends AppCompatActivity {

    public static ApiFirebase apiFirebase;
    RecyclerView recyclerView;
    ComicAdapter comicAdapter;
    FloatingActionButton fabtnAddComic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Theme_Launcher);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        apiFirebase = new ApiFirebase("comicsdatabase");

        recyclerView = findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        comicAdapter = new ComicAdapter(apiFirebase.options);
        comicAdapter.setContext(getApplicationContext());
        recyclerView.setAdapter(comicAdapter);

        setUpRecyclerView();

        //insertMoks();

        fabtnAddComic = findViewById(R.id.fabtnAdd);
        fabtnAddComic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, CreateUpdateActivity.class);
                startActivity(intent);
                make(view, "Click btn add!", LENGTH_SHORT)
                        .setAction("Action", null).show();
            }
        });
    }

    //para el comicAdapter firebase
    @Override
    protected void onStart() {
        super.onStart();
        comicAdapter.startListening();
    }
    //para el comicAdapter firebase
    @Override
    protected void onStop() {
        super.onStop();
        comicAdapter.stopListening();
    }

    //para borrar al deslizar el item
    private void setUpRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new SwipeToDeleteCallback(comicAdapter));
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    public void insertMoks() {
        String[] title = {"Mazinger Z","X-force","Batman"};
        String[] author = {"Mazinger","Fandom","Murdoc"};
        String[] img = {"https://todomazinger.com/wp-content/uploads/2017/10/mazinger-comic-tapadura2-300x300.jpg",
                "https://static.wikia.nocookie.net/marveldatabase/images/3/33/X-Force_Vol_1_1.jpg/revision/latest/top-crop/width/300/height/300?cb=20180113051237",
                "https://hablemosdecomics.es/wp-content/uploads/2019/04/DETECTIVE-1000-300x300.jpg"};

        for (int i = 0; i < title.length; i++) {
            apiFirebase.insert(new Comic("", title[i], author[i], img[i], 4f));
        }
    }

}
