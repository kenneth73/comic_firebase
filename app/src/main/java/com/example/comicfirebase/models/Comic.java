package com.example.comicfirebase.models;

import java.io.Serializable;

public class Comic implements Serializable {
    private String id;
    private String title;
    private String author;
    private String img;
    private float rating;

    public Comic () {}

    public Comic(String id, String title, String author, String img, float rating) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.img = img;
        this.rating = rating;
    }

    public static final String attrTitle = "title";
    public static final String attrAuthor = "author";
    public static final String attrImg = "img";
    public static final String attrRating = "rating";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }
}
