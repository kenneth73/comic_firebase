package com.example.comicfirebase.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.comicfirebase.CreateUpdateActivity;
import com.example.comicfirebase.MainActivity;
import com.example.comicfirebase.R;
import com.example.comicfirebase.models.Comic;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import static com.google.android.material.snackbar.Snackbar.make;

public class ComicAdapter extends FirebaseRecyclerAdapter<Comic, ComicAdapter.ComicHolder> {

    //private Comic comic; //podriamos guardar el comic borrado

    public Context context;
    public View view; // para poder utilizar view en la funcion showUndoSnackbar()

    public ComicAdapter(@NonNull FirebaseRecyclerOptions<Comic> options) {super(options);}

    @Override
    protected void onBindViewHolder(@NonNull ComicAdapter.ComicHolder holder, int position, @NonNull Comic model) {
        holder.bind(model);
        //this.comic = model;
    }

    @NonNull
    @Override
    public ComicHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view_relative, parent, false);//item_view_relative
        this.view = v;
        return new ComicHolder(v);
    }

    public class ComicHolder extends RecyclerView.ViewHolder{
        ImageView imageViewComic;
        TextView textViewTitle;
        TextView textViewAuthor;
        //icon star
        TextView textViewRatingNumber;

        public ComicHolder(@NonNull View itemView) {
            super(itemView);
            imageViewComic= itemView.findViewById(R.id.imageViewComic);
            textViewTitle= itemView.findViewById(R.id.textViewTitleItemValue);
            textViewAuthor= itemView.findViewById(R.id.textViewAuthorItemValue);
            textViewRatingNumber= itemView.findViewById(R.id.textViewRatingValue);
        }

        public void bind(Comic model) {

            try {
                Picasso.with(imageViewComic.getContext())
                        .load(model.getImg())
                        .into(imageViewComic);
            } catch (Exception e) {
                Log.e("ComicAdapter", e.toString());
            }


            textViewTitle.setText(model.getTitle());
            textViewAuthor.setText(model.getAuthor());
            textViewRatingNumber.setText(String.valueOf(model.getRating()));
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String id = model.getId();
                    String title = model.getTitle();
                    String author = model.getAuthor();
                    make(v, id+title+author, BaseTransientBottomBar.LENGTH_SHORT)
                            .setAction("Action", null).show();

                    Intent intent = new Intent(v.getContext(), CreateUpdateActivity.class);
                    intent.putExtra("comic", model);
                    v.getContext().startActivity(intent);
                }
            });
        }
    }

    public void deleteItem(int position) {
        Comic c = getItem(position);
        MainActivity.apiFirebase.delete(c);
        notifyItemRemoved(position); // si lo comentamos funciona igualmente
        //showUndoSnackbar(position);
    }
    public void showUndoSnackbar(int position) {

        Snackbar snackbar = Snackbar.make(this.view, "Esta seguro?", Snackbar.LENGTH_LONG);
        snackbar.setAction("Si", v -> deleteItem(position));
        snackbar.show();
    }


    private void undoDelete(int position) {
        deleteItem(position);
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
