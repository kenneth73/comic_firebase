package com.example.comicfirebase;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.comicfirebase.models.Comic;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.squareup.picasso.Picasso;

public class CreateUpdateActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView titleActivity;
    private ImageView imageView;
    private TextInputLayout titleLayout;
    private TextInputEditText titleInput;
    private TextInputLayout authorLayout;
    private TextInputEditText authorInput;
    //private TextInputLayout urlImageLayout;
    private TextInputEditText urlImageInput;
    private RatingBar ratingBar;
    private Button btnUpdate;
    private Button btnCheckImg;
    private Comic comic;
    private boolean isCreate = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_update_layout);
        titleActivity = findViewById(R.id.textViewTitleActivity);
        imageView = findViewById(R.id.imageViewComicCreate);
        titleLayout = findViewById(R.id.title_input_layout);
        titleInput = findViewById(R.id.title_input);
        authorLayout = findViewById(R.id.author_input_layout);
        authorInput = findViewById(R.id.author_input);
        //urlImageLayout = findViewById(R.id.url_image_input_layout);
        urlImageInput = findViewById(R.id.url_image_input);
        ratingBar = findViewById(R.id.ratingBar);
        btnUpdate = findViewById(R.id.btnSave);
        btnCheckImg = findViewById(R.id.btnCheckImg);

        btnUpdate.setOnClickListener(this);
        btnCheckImg.setOnClickListener(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            isCreate = false;
            comic = (Comic) bundle.get("comic");
            //Log.println(1, "TAAAAAAG:",comic.getAuthor() );
            titleActivity.setText("Update Comic");
            titleInput.setText(comic.getTitle());
            authorInput.setText(comic.getAuthor());
            urlImageInput.setText(comic.getImg());
            ratingBar.setRating(comic.getRating());
            //render image
            try {
                Picasso.with(imageView.getContext())
                        .load(comic.getImg())
                        .into(imageView);
            } catch (Exception e) {
                Log.e("ComicAdapter", e.toString());
            }

        } else {
            titleActivity.setText("Create Comic");
            ratingBar.setRating(2.5f);
            //render image
            try {
                Picasso.with(imageView.getContext())
                        .load(getString(R.string.image_url_noimage)) //si utilizo getString(R.id.image_url_noimage) avecen no carga la imagen!
                        .into(imageView);
            } catch (Exception e) {
                Log.e("ComicAdapter", e.toString());
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSave:
                String author = authorInput.getText().toString();
                String title = titleInput.getText().toString();
                float rating = Float.parseFloat(String.valueOf(ratingBar.getRating()));

                boolean correctForm = true;
                if (author.length() < 1) {
                    authorLayout.setError("Author is required");
                    correctForm = false;
                }
                if (title.length() < 1) {
                    titleLayout.setError("Title is required");
                    correctForm = false;;
                }
                if (!correctForm) return;

                if (isCreate) {
                    String urlImage =urlImageInput.getText().toString();//recojemos la url del comic, si esta vacia, cambiamos la url para no mostrar foto vacia
                    if (urlImage.length()<1) {
                        urlImage = getString(R.string.image_url_noimage);
                    }
                    comic = new Comic("", authorInput.getText().toString(), titleInput.getText().toString(), urlImage, rating );
                    MainActivity.apiFirebase.insert(comic);
                    Intent intent = new Intent(CreateUpdateActivity.this, MainActivity.class);
                    startActivity(intent);
                } else {
                    comic.setAuthor(authorInput.getText().toString());
                    comic.setTitle(titleInput.getText().toString());
                    comic.setImg(urlImageInput.getText().toString());
                    comic.setRating(rating);
                    MainActivity.apiFirebase.update(comic);
                    Intent intent = new Intent(CreateUpdateActivity.this, MainActivity.class);
                    startActivity(intent);
                }
                break;
            case R.id.btnCheckImg:
                String urlImage = urlImageInput.getText().toString();
                if (!(urlImage.length()>1))  urlImage = getString(R.string.image_url_noimage);
                try {
                    Picasso.with(imageView.getContext())
                            .load(urlImage)
                            .into(imageView);
                } catch (Exception e) {
                    Log.e("ComicAdapter", e.toString());
                }
                break;
        }
    }
}
